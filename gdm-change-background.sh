#!/bin/bash
set -e
IMAGE=$1

if [ "$(id -u)" != "0" ]
then
  echo "Please run this as root" 
  exit 98
fi

if [ -z "$1" ]
then
  echo "Usage: $(basename $0) < PATH_TO_IMAGE | original >"
  exit 99
fi

if [ "${IMAGE}" = "original" ]
then
  echo "Restoring original theme"
elif [ ! -f "${IMAGE}" ]
then
  echo "File ${IMAGE} does not exist, bailing."
  exit 98
fi

GNOME_SHELL_THEME=/usr/share/gnome-shell/gnome-shell-theme.gresource
GRESOURCE=$(basename $GNOME_SHELL_THEME)

if [ -x $(which mktemp) ]
then
  WORKDIR=$(mktemp -d)
else
  WORKDIR="/tmp/$(basename $0).$$"
fi

if [ ! -d "${WORKDIR}/theme" ]
then
  mkdir -p ${WORKDIR}/theme
fi
cd $WORKDIR

for FILE in `gresource list $GNOME_SHELL_THEME`; do
  gresource extract $GNOME_SHELL_THEME $FILE > ${WORKDIR}/$(echo $FILE | sed -e 's#^/org/gnome/shell/##g')
done

cd theme
if [ "${IMAGE}" != "original" ]
then
  cp -f "${IMAGE}" ./
fi

echo "
#lockDialogGroup {
  background: #2e3436 url(resource:///org/gnome/shell/theme/$(basename $IMAGE));
  background-size: cover;
  background-repeat: no-repeat;
}" >>gnome-shell.css

echo '<?xml version="1.0" encoding="UTF-8"?>
<gresources>
  <gresource prefix="/org/gnome/shell/theme">' >"${GRESOURCE}.xml"
for FILE in `ls *.*`; do
  echo "    <file>${FILE}</file>" >>"${GRESOURCE}.xml"
done
echo '  </gresource>
</gresources>' >>"${GRESOURCE}.xml"

glib-compile-resources "${GRESOURCE}.xml"

if [ ! -f "/usr/share/gnome-shell/${GRESOURCE}.orig" ]
then 
  cp "/usr/share/gnome-shell/$GRESOURCE" "/usr/share/gnome-shell/${GRESOURCE}.orig"
fi

mv "/usr/share/gnome-shell/$GRESOURCE" "/usr/share/gnome-shell/${GRESOURCE}.$(date '+%Y%m%d%H%M%S')"
mv "$GRESOURCE" /usr/share/gnome-shell/

if [ "$(getenforce | grep -ic enforcing)" = "1" ]
then
  restorecon -Rv /usr/share/gnome-shell
fi

rm -r $WORKDIR

echo
echo "Theme changed. Restart gdm (or simply reboot) to see the new theme"
echo
